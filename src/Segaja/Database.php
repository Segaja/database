<?php
namespace Segaja;

use Doctrine;

class Database
{
    /**
     * @var Doctrine\DBAL\Connection
     */
    private $database;

    /**
     * @param Doctrine\DBAL\Connection $database
     */
    public function __construct(Doctrine\DBAL\Connection $database)
    {
        $this->database = $database;
    }

    /**
     * Executes a sql action query. Executes a sql query which makes modifications at the database and which has no
     * return value (e.g.: ALTER, INSERT, DELETE, CALL, etc.).
     *
     * @param string $sql
     * @param array  $params
     * @param array  $types
     *
     * @return int
     *
     * @throws Doctrine\DBAL\DBALException
     */
    public function actionQuery(string $sql, array $params, array $types) : int
    {
        return $this->database->executeUpdate($sql, $params, $types);
    }

    /**
     * Executes a sql SELECT query and returns the multi line response. This method exists to execute sql SELECT
     * queries where it is known, that the result will be more than one record.
     *
     * @param string $sql    the query to be executed
     * @param array  $params array with the variables for the prepared statement
     * @param array  $types  array with variable types to the $params array
     *
     * @return array each array index is one record of the return value of the mysql query
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function multiSelectQuery(string $sql, array $params, array $types) : array
    {
        $statement = $this->database->executeQuery($sql, $params, $types);
        $result    = $statement->fetchAll();

        $statement->closeCursor();

        return $result;
    }

    /**
     * Executes a sql SELECT query and returns the first record of the result set. This method exists to execute sql
     * SELECT queries where it is known, that the result will have only one record.
     *
     * @param string $sql    the query to be executed
     * @param array  $params array with the variables for the prepared statement
     * @param array  $types  array with variable types to the $params array
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function singleSelectQuery(string $sql, array $params, array $types)
    {
        if (false === \strripos($sql, 'LIMIT 1')) {
            $sql .= ' LIMIT 1';
        }

        $statement = $this->database->executeQuery($sql, $params, $types);
        $return    = $statement->fetchAll();

        $statement->closeCursor();

        if (0 === \count($return)) {
            return null;
        }

        return $return[0];
    }

    /**
     * Returns the last ID.
     *
     * @return int
     */
    public function insertId() : int
    {
        return (int) $this->database->lastInsertId();
    }
}
