# Segaja/Database

## installation
```
composer require segaja/database
```

## Usage

```php
<?php
// example database config
$databaseConfig = [
    'charset'       => 'utf8',
    'dbname'        => 'database_name',
    'driver'        => 'pdo_mysql',
    'driverOptions' => [
        \PDO::ATTR_CASE               => \PDO::CASE_NATURAL,
        \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_ORACLE_NULLS       => \PDO::NULL_TO_STRING,
        \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
    ],
    'host'          => 'database_hostname',
    'password'      => 'database_password',
    'user'          => 'database_user',
];

$dbalConnection = Doctrine\DBAL\DriverManager::getConnection(
    $databaseConfig,
    new Doctrine\DBAL\Configuration()                                                                  
)

$database = new \Segaja\Database($dbalConnection);
```

## Methods

### `insertId() : int`
Returns the last insert id of the database connection.

### `actionQuery(string $sql, array $params, array $types) : int`
Executes the given SQL query (`$sql`) with the given parameters and types.
The parameters and types need to have the same amount of entries.
The call is passed to the `executeUpdate()` function of the `\Doctrine\DBAL\Connection`.
The method returns the amount of effected rows.

### `multieSelectQuery(string $sql, array $params, array $types) : array`
This method proxies the `\Doctrine\DBAL\Connection::executeQuery()` method.
It executes the method and then calls the `fetchAll()` method on the returned statement
and returns the result from that call.

### `singleSelectQuery(string $sql, array $params, array $types) : array`
This method is very similar to the `multiSelectQueriy()` method but it adds a " LIMIT `" to the end of the
query (`$sql`) if it is not present.
Once they statement `fetchAll()` method was called this method returns `null` if the query didn't yield any
results or the first entry of the result set.
